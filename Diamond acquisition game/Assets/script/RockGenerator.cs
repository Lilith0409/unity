﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockGenerator : MonoBehaviour
{
    public GameObject RockPrefab;
    float span = 0.3f;//追加される間隔
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(RockPrefab) as GameObject;
            int px = Random.Range(-4, 4);
            go.transform.position = new Vector3(8, px, 0);

        }
    }
}
