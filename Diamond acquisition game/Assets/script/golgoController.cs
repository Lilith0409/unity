﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class golgoController : MonoBehaviour
{
    GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        this.Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.01f, 0, 0);
        //左に等速で移動
        if (transform.position.x < -8.0f)
        {
            Destroy(gameObject);
        }
        //座標外にでたらオブジェクト破棄

        //当たり判定
        Vector2 p1 = transform.position;
        Vector2 p2 = this.Player.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.2f;
        float r2 = 1.0f;

        if (d < r1 + r2)
        {
            GameObject.Find("Canvas").GetComponent<ScoreDirector>().golgoScore();
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().golgoScore();
            Destroy(gameObject);
        }
    }
}
