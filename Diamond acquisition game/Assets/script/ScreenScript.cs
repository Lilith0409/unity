﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.005f, 0, 0);

        if(transform.position.x < -25.68f)
        {
            transform.position = new Vector3(25.68f, 0, 0);
        }
    }
}
