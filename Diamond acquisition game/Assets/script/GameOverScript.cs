﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    int endScore;
    // Start is called before the first frame update
    void Start()
    {
        endScore = ScoreDirector.getScore();
        GetComponent<Text>().text = "最終スコア:" + endScore;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {

            SceneManager.LoadScene("GameScene");
        }
    }
}
