﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameDirector : MonoBehaviour
{
   GameObject hp;
   int hpflag = 10;

    // Start is called before the first frame update
    void Start()
    {
        this.hp = GameObject.Find("hp");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Hp()
    {
        this.hp.GetComponent<Image>().fillAmount -= 0.1f;
        hpflag -= 1;
        if (hpflag == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void golgoScore()
    {
        this.hp.GetComponent<Image>().fillAmount += 1.0f;
        hpflag = 10;
    }
}
